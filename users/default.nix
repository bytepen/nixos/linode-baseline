{ config, pkgs, ... }:
{
  users = {
    defaultUserShell = pkgs.zsh;
    mutableUsers = false;
    groups = {
      bamhm182 = {};
      deploy = {};
    };
    users = {
      bamhm182 = {
        isNormalUser = true;
        home = "/home/bamhm182";
        description = "Dylan Wilson";
        group = config.users.groups.bamhm182.name;
        shell = pkgs.zsh;
        initialHashedPassword = "!";
        hashedPassword = "!";
        extraGroups = [ "wheel" "networkmanager" ];
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJoQ5pUDTCIo9X/Ldsw/Td3wQsTGovc3/z8o6QxZwf3f"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICOgFaPH1B9U5Up20W7x0IMGsXRXWP9kGcNAQQ0AdcDG"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHCKWKj3hH6PJZL9ueWq6SK5vEM5Ib68kZGgWCvMQiFB"
          "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAICiqKerr8r3dkpmWUom+DWBDpZsZpjHtNAWBt3aIQ5AWAAAAGXNzaDpkeWxhbkBieXRlcGVuLmNvbS15a3A="
          "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIMh6UbPH87TFIklNGfYHQuPSNpdcdokDUH0XwJ/EzvEZAAAAGXNzaDpkeWxhbkBieXRlcGVuLmNvbS15azI="
          "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAICQi/pXr/XXkfaj5HogQz+VPVpXIPqkU/RyaiNw8yPR3AAAAGXNzaDpkeWxhbkBieXRlcGVuLmNvbS15azE="
        ];
      };
      deploy = {
        isSystemUser = true;
        group = config.users.groups.deploy.name;
        initialHashedPassword = "!";
        hashedPassword = "!";
        openssh.authorizedKeys.keys = [
          "restrict ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJoQ5pUDTCIo9X/Ldsw/Td3wQsTGovc3/z8o6QxZwf3f"
          "restrict ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICOgFaPH1B9U5Up20W7x0IMGsXRXWP9kGcNAQQ0AdcDG"
          "restrict ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHCKWKj3hH6PJZL9ueWq6SK5vEM5Ib68kZGgWCvMQiFB"
          "restrict sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAICiqKerr8r3dkpmWUom+DWBDpZsZpjHtNAWBt3aIQ5AWAAAAGXNzaDpkeWxhbkBieXRlcGVuLmNvbS15a3A="
          "restrict sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIMh6UbPH87TFIklNGfYHQuPSNpdcdokDUH0XwJ/EzvEZAAAAGXNzaDpkeWxhbkBieXRlcGVuLmNvbS15azI="
          "restrict sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAICQi/pXr/XXkfaj5HogQz+VPVpXIPqkU/RyaiNw8yPR3AAAAGXNzaDpkeWxhbkBieXRlcGVuLmNvbS15azE="
        ];
      };
      root = {
        initialHashedPassword = "!";
        hashedPassword = "!";
      };
    };
  };
}
