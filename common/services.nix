{ config, pkgs, lib, ... }:
{
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      PermitRootLogin = "no";
      X11Forwarding = false;
    };
    authorizedKeysFiles = lib.mkForce [ "/etc/ssh/authorized_keys.d/%u" ];
    allowSFTP = false;
    extraConfig = ''
      AuthenticationMethods publickey
      AllowUsers ${config.users.users.bamhm182.name} ${config.users.users.deploy.name}
    '';
  };
}
