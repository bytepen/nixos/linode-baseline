{ config, modulesPath, ... }:
{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")

    ./boot.nix
    ./disks.nix
    ./packages.nix
    ./networking.nix
    ./security.nix
    ./services.nix
    ./system.nix
  ];
}
