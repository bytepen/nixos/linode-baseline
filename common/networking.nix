{ config, pkgs, ... }:
{
  networking = {
    usePredictableInterfaceNames = false;
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
    enableIPv6 = false;
  };
}
