{ config, pkgs, ... }:
{
  environment.systemPackages = [
    pkgs.git
    pkgs.vim
    pkgs.wget
    pkgs.inetutils
    pkgs.mtr
    pkgs.sysstat
    pkgs.tmux
  ];

  programs.zsh.enable = true;
}
