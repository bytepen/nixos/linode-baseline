{ config, lib, pkgs, ... }:
{
  security = {
    sudo = {
      enable = true;
      extraConfig = ''
        Defaults lecture      = never
        Defaults passwd_tries = 10
      '';
      execWheelOnly = false;
      extraRules = [
        {
          users = [ config.users.users.deploy.name ];
          runAs = config.users.users.root.name;
          commands = [
            {
              command = "/nix/store/*-nixos-system-*/bin/switch-to-configuration";
              options = [ "NOPASSWD" ];
            }
            {
              command = "/nix/store/*-nix-*/bin/nix-env";
              options = [ "NOPASSWD" ];
            }
          ];
        }
      ];
    };
    pam = {
      services = {
        sudo.text = ''
          account required pam_unix.so
          auth sufficient ${pkgs.pam_ssh_agent_auth}/libexec/pam_ssh_agent_auth.so file=/etc/ssh/authorized_keys.d/%u
          auth required pam_deny.so
          password sufficient pam_unix.so nullok yescrypt
          session required pam_env.so conffile=/etc/pam/environment readenv=0
          session required pam_unix.so
        '';
      };
      enableSSHAgentAuth = true;
    };
  };
}
