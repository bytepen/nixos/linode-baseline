{ config, ... }:
{
  boot = {
    kernelParams = [ "console=ttyS0,19200n8" ];
    kernelModules = [ ];
    extraModulePackages = [ ];
    initrd = {
      availableKernelModules = [ "virtio_pci" "virtio_scsi" "ahci" "sd_mod" ];
      kernelModules = [ ];
    };
    loader = {
      timeout = 10;
      grub = {
          enable = true;
          extraConfig = ''
            serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
            terminal_input serial;
            terminal_output serial;
          '';
          forceInstall = true;
          device = "nodev";
      };
    };
  };
}
